﻿// ConsoleApplicationZibiTest1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stddef.h>
#include "pch.h"
#include <iostream>
#include "nolclientapi.h"


//FUNCKJE ZWROTNE

void onlinecallback(NOL3Client::NolRecentInfo *info) // Funkcja zwrotna dla danych online
{
	if(info->BitMask & 0x0080)
		printf("Bid: %d", info->Bid);
}

void outlookcallback(const char* news) // funkcja zwrotna dla wizjera
{
	printf(news);
}

/*
void accountcallback(NOL3Client::NolAggrStatement* state) // funkcja zwrotna dla wyciągu
{
	for (int i = 0; i < state->size; i++) // state->size – ilość kont
	{
		printf(state->ptrstate[i].name); // ptrstate[i].name – nazwa konta
		for (int j = 0; j < state->ptrstate[i].sizefund; j++) // ptrstate[i].sizefund – ilość funduszy
		{
			printf("%s: %s",state->ptrstate[i].ptrfund[j].name, state->ptrstate[i].ptrfund[j].value);
		}
		for (int j = 0; j < state->ptrstate[i].sizepos; j++) // ptrstate[i].sizepos – ilość aktywów
		{
			printf("%s: %s",state->ptrstate[i].ptrpos[j].isin,printf(state->ptrstate[i].ptrpos[i].acc110);
		}
	}
}


void ordercallback(NOL3Client::NolOrderReport* order) // funkcja zwrotna dla zleceń
{
	if(order->BitMask & 0x0400)
		printf(order->ticker.Isin);
	if(order->BitMask & 0x0040)
		printf(order->Stat);

}
*/

int main()
{
    std::cout << "Hello World!\n";

	int odp = 0;
	//int odp = NOL3Client::SetCallbackAccount(accountcallback); // ustawienie f.zwrotnej dla wyciągu
	//odp     = NOL3Client::SetCallbackOrder(ordercallback);         // ustawienie f.zwrotnej dla zleceń,transakcji
	odp     = NOL3Client::SetCallbackOutlook(outlookcallback);     //ustawienie f.zwrotnej dla wizjera

	odp = NOL3Client::Initialize("BOS;BOS");

	// FUNKCJE DOTYCZĄCE NOTOWAŃ ONLINE
	odp = NOL3Client::SetCallback(onlinecallback); //ustawienie f.zwrotnej dla notowań
	odp = NOL3Client::AddToFilter("PKOBP;KGHM", true); //dodanie do filtra walorów, true – symbol
	// wyczyszczenie filtra, dodanie waloru, false - isin
	odp = NOL3Client::RemFromFilter("PL9999999995", false);
	odp = NOL3Client::ClearFilter(); // wyczyszczenie filtra

	// FUNKCJA DOTYCZĄCA ZLECEŃ
	//NOL3Client::NolOrderRequest* order = new NOL3Client::NolOrderRequest(); // obiekt zlecenia
	// w order należy podać wszystkie wymagane pola
	//NOL3Client::NolOrderReport* report = new NOL3Client::NolOrderReport(); // obiekt raportu
	// złożenie nowego zlecenia ; NOL3Client::NewOrder – nowe zlecenie
	//NOL3Client::ModOrder – modyfikacja zlecenia, NOL3Client::DelOrder – anulowanie zlecenia
	//odp = NOL3Client::APIOrderRequest(order, report, NOL3Client::NewOrder);


	//Dalej:


	// ZAMYKANIE
	odp = NOL3Client::Shutdown(); // zamknięcie

}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
